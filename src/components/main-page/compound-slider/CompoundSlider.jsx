import React, { useEffect, useState } from "react";
import "./CompoundSlider.css"
import { useQuery } from "react-query";
import { fetchGeneric } from "../../../javascript/Apis";
import Spinner from "../../../utilities/spinner/Spinner";
import MainSliderContainer from "./main-slider-container/MainSliderContainer";
import MinSliderContainer from "./min-slider-container/MinSliderContainer";
import { useNavigate } from "react-router-dom";
import Error from "../../../utilities/error/Error";
const CompoundSlider = (props)=>{
  
// useEffect(()=>{
//     const interval = setInterval(()=>{
//       next()
//     },5000)
//     return ()=>clearInterval(interval)
//   })
  const [size,setSize] = useState(window.innerWidth)
  const [move,setMove] = useState(-1)
  const updateMedia = () => {
    setSize(window.innerWidth > 768);
  };
  useEffect(() => {
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  });

  
  // const prev = () =>{
  //   cont === 0 ? setCont(slides.length-1) : setCont(cont-1)
  // }

  // const next = () =>{
  //   setMove(true)
  //   cont === slides.length-1 ? setCont(0) : setCont(cont+1)
  // }
  // const url="&page_size=16"
  // const query = useQuery(["games",url],()=>fetchGeneric(url))

  // if(query.isFetching){
  //   return <Spinner/>
  // }

  // if(query.isError){
  //   return <h1>Error</h1>
  // } 

  
  
  
  const query_a = useQuery([props.query_key_a,props.url],()=>fetchGeneric(props.url_a))

  const query_b = useQuery([props.query_key_b,props.url_b],()=>fetchGeneric(props.url_b))

  const navigate = useNavigate()
  


  // const set_game_data = (id)=>{
  //   setGame(id)
  //   navigate("/game")
  // }


  if(query_a.isLoading || query_b.isLoading){return <Spinner/>}

  if(query_a.isError || query_b.isError){return <Error/>}

  return query_a.isFetching
  ?<Spinner/>
  :size ? <MainSliderContainer games_array_a = {query_a.data} games_array_b = {query_b.data}  />:<MinSliderContainer games_array_a = {query_a.data} games_array_b = {query_b.data} />

}

export default CompoundSlider