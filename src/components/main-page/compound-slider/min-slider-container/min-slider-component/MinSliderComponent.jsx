import React, { useState } from 'react'
import "./MinSliderComponent.css"

import { useNavigate } from "react-router-dom";
const MinSliderComponent = (props) => {
    const [margin,setMargin] = useState(0)
    const buttons = [{id:0},{id:1},{id:2}]
    const move=(margin)=>{
        setMargin(margin)
    }
    const navigate = useNavigate()

    const set_game_data = (id)=>{

        navigate(`/game/${id}`)
      }
    return (
    <div className="min-slider-container">
        <div className="min-slider" style={{marginLeft:`-${margin}00%`}}>
            <div className="min-slider-content">
                {
                    props.array.results.map((card,idx)=>
                    <div className="slide" key={idx} style={{backgroundImage:`url(${card.background_image})`}} onClick={()=>set_game_data(card.id)} ></div>
                    )
                }
            </div>
        </div>
        <div className="btn-container">
            {
                buttons.map((button,idx)=>
                <div key={idx} className={button.id===margin?"btn active":"btn"} onClick={()=>move(button.id)}></div>
                )
            }
        </div>
    </div>
    )
}

export default MinSliderComponent