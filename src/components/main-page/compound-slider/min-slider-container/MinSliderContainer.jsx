import React, { useEffect } from 'react'
import MinSliderComponent from './min-slider-component/MinSliderComponent'
import AOS from "aos";
import "aos/dist/aos.css";

const MinSliderContainer = (props) => {
  useEffect(() => {
    AOS.init({duration:1000,once: false,});
    AOS.refresh();
  }, []);
  return (
    <section data-aos="fade-up" className='min-sliders'>
        <MinSliderComponent array = {props.games_array_b} />
        <MinSliderComponent array = {props.games_array_a} />
    </section>
  )
}

export default MinSliderContainer