
import "./MainSliderContainer.css"
import Debounce from '../../../../javascript/Debounce'
import React, { useEffect } from "react";
import { BsChevronRight,BsChevronLeft } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import AOS from "aos";
import "aos/dist/aos.css";
const ManSliderContainer = (props) => {
  useEffect(() => {
    AOS.init({duration:1000});
    AOS.refresh();
  }, []);
  const navigate = useNavigate()

  const get_slider = ()=>{
    let slider = document.querySelector(".slider-container")
    return slider
  }
  const next=()=>{
    let slider = get_slider()
    slider.style.transition="1s"
    slider.style.marginLeft="-200%"
    slider.children[2].style.opacity="1"
    setTimeout(() => {
      slider.style.transition="none"
      slider.insertAdjacentElement('beforeend',slider.children[0])
      slider.style.marginLeft="-100%"
      slider.children[2].style.opacity=".4"
    }, 900);
  }
  const moveNext = Debounce(next,500)
  const prev=()=>{
    let slider = get_slider()
    slider.style.transition="1s"
    slider.style.marginLeft="0%"   
    slider.children[2].style.opacity="1"
    setTimeout(() => {
      slider.style.transition="none"
      slider.insertAdjacentElement('beforeend',slider.children[0])
      slider.style.marginLeft="-100%"
      slider.children[2].style.opacity=".4"
    }, 900);
  }
  const movePrev= Debounce(prev,500)
  const set_game_data = (id)=>{
    navigate(`/game/${id}`)
  }

  return (
    <section data-aos="fade-up" className="slider-2-2-6-container">
            <div className="slider-2-2-6">
              <h3>Games</h3>
              <br />
              <h4>and more games</h4>
              <div className="controls">
                <div className="btn" onClick={()=>movePrev()}><BsChevronLeft className="icon"/></div>
                <div className="btn" onClick={()=>moveNext()}><BsChevronRight className="icon"/></div>
            </div>
            <div className="slider-container">
                <div className="slider-content">
                  {
                    props.games_array_a.results.map((card,idx)=>
                    <div className="slide" key={idx} style={{backgroundImage:`url(${card.background_image})`}} onClick={()=>set_game_data(card.id)} ></div>
                    )
                  }
                </div>
                <div className="slider-content">
                  {
                    props.games_array_b.results.map((card,idx)=>
                    <div className="slide" key={idx} style={{backgroundImage:`url(${card.background_image})`}} onClick={()=>set_game_data(card.id)} ></div>
                    )
                  }                
                </div>
                <div className="slider-content">
                  {
                    props.games_array_a.results.map((card,idx)=>
                    <div className="slide" key={idx} style={{backgroundImage:`url(${card.background_image})`}} onClick={()=>set_game_data(card.id)} ></div>
                    )
                  }
                </div>
                <div className="slider-content">
                  {
                    props.games_array_b.results.map((card,idx)=>
                    <div className="slide" key={idx} style={{backgroundImage:`url(${card.background_image})`}} onClick={()=>set_game_data(card.id)}></div>
                    )
                  }                
                </div>
            </div>
          </div>
    </section>
  )
}

export default ManSliderContainer