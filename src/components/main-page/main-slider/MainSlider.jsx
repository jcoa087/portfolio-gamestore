import React, { useEffect, useState } from "react";
import "./MainSlider.css"
import { FiChevronRight,FiChevronLeft } from "react-icons/fi";
import { useQuery } from "react-query";
import {fetchGeneric} from "./../../../javascript/Apis.js"
import { useNavigate } from "react-router-dom";
import Spinner from "./../../../utilities/spinner/Spinner";
import ActiveBar from "../../../utilities/bars/active_bar/ActiveBar";
import Bar from "../../../utilities/bars/bar/Bar";
import Error from "../../../utilities/error/Error";
const MainSlider = ()=>{
  useEffect(()=>{
    const interval = setInterval(()=>{
        next()
      },5000)
      return ()=>clearInterval(interval)
  })

  const [cont,setCont] = useState(0)


  const prev = () =>{
    cont === 0 ? setCont(slides.length-1) : setCont(cont-1)
  }
  const next = () =>{
    cont === slides.length-1 ? setCont(0) : setCont(cont+1)
  }


  const navigate = useNavigate()

  const setGameData = ()=>{
    navigate(`/game/${query.data.results[cont].id}`)
  }
  const url="&page=6&page_size=5&genres=2"
  const query = useQuery(["game-slider",url],()=>fetchGeneric(url))
  
  if(query.isFetching){return <Spinner/>}

  if(query.isError){return <Error/>}

  const slides = query.data.results
  // const slides = [{url:"https://image.api.playstation.com/vulcan/ap/rnd/202210/0706/EVWyZD63pahuh95eKloFaJuC.png"},{url:"https://image.api.playstation.com/vulcan/ap/rnd/202206/0207/V6IViuKogBMRtajqjnYrcj0e.png"},
  //                 {url:"https://image.api.playstation.com/vulcan/ap/rnd/202206/0206/WmriZBRlSeXWEEDLJOWW7MdW.png"},{url:"https://image.api.playstation.com/vulcan/ap/rnd/202206/0204/uDFoGvnMTTCLVmTwjj0njGWC.png"},
  //                 {url:"https://image.api.playstation.com/vulcan/ap/rnd/202101/0812/FkzwjnJknkrFlozkTdeQBMub.png"}]
  const slider = ()=>{
      return (
        <section className="main-slider">

          <div className="slide" style={{backgroundImage:`url(${slides[cont].background_image})`}} >
            <div to="/game" onClick={()=>setGameData()} className="link">view more</div>
          </div>
          <button onClick={prev} className="left"><FiChevronLeft/></button>
          <button onClick={next} className="right"><FiChevronRight/></button>
          <div className="controls">
            {
              slides.map((item,idx)=>
                cont === idx ? <ActiveBar  key={idx} onClick={()=>setCont(idx)} /> : <Bar key={"hollow-"+idx} onClick={()=>setCont(idx)} />
              )
            }
          </div>
      </section>
      )   
  }
  // return slider()
  return query.isFetching
  ?<Spinner/>
  :slider()
}
export default MainSlider