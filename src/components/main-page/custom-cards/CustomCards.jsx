import React from "react";
import "./CustomCards.css"
import { useQuery } from 'react-query'
import Spinner from '../../../utilities/spinner/Spinner'
import { useNavigate } from "react-router-dom";
import { useSeriesContext } from "../../../context/Context";
import {fetchGeneric}  from "../../../javascript/Apis"
import { BsChevronRight } from "react-icons/bs";
import Error from "../../../utilities/error/Error";
const CustomCards = (props) =>{

  const query = useQuery([props.query_key,props.url],()=>fetchGeneric(props.url))

  const navigate = useNavigate()
  
  const set_game_data = (id)=>{

    navigate(`/game/${id}`)
  }

  const [series,setSeries] = useSeriesContext()

  const set_series_data= ()=>{
    const series = {url:props.series_url,query_key_series:props.query_key_series}
    setSeries(series)
    navigate("/series")
  }

  if(query.isLoading){return <Spinner/>}

  if(query.isError){return <Error/>}

  const component=()=>{  
    return(
      <section className="custom-cards-container" style={{backgroundColor:`${props.background_color}`}}>
        <div className="custom-cards-content"  style={{color:`${props.color}`}}>
          <div className="series-link" onClick={()=>set_series_data()}>
            <div className="title">
            {props.title}
            </div>
            <BsChevronRight className="series-arrow" />
        </div>
        <div className="custom-cards">
        {
            query.data.results.map((card,idx)=>
            <div key={idx} className="card" onClick={()=>set_game_data(card.id)}>
              <div className="img-container" style={{backgroundImage:`url(${card.background_image})`}}>
                <div className="img-container-overlay"></div>
              </div>
              <div className="description-container" >
                {card.name}
              </div>
            </div>
            )
          }
            {/* <div  className="card" >
              <div className="img-container" style={{backgroundImage:`url(https://gmedia.playstation.com/is/image/SIEPDC/call-of-duty-modern-warfare-2-pack-01-ps4-ps5-en-26may22$en?$1200px$)`}}>
                <div className="img-container-overlay"></div>
              </div>
              <div className="description-container" >
                Call of Duty: Modern Warfare 2
              </div>
            </div>
            <div  className="card" >
              <div className="img-container" style={{backgroundImage:`url(https://gmedia.playstation.com/is/image/SIEPDC/call-of-duty-modern-warfare-2-pack-01-ps4-ps5-en-26may22$en?$1200px$)`}}>
                <div className="img-container-overlay"></div>
              </div>
              <div className="description-container" >
                Call of Duty: Modern Warfare 2
              </div>
            </div>
            <div  className="card" >
              <div className="img-container" style={{backgroundImage:`url(https://gmedia.playstation.com/is/image/SIEPDC/call-of-duty-modern-warfare-2-pack-01-ps4-ps5-en-26may22$en?$1200px$)`}}>
                <div className="img-container-overlay"></div>
              </div>
              <div className="description-container" >
                Call of Duty: Modern Warfare 2
              </div>
            </div>
            <div  className="card" >
              <div className="img-container" style={{backgroundImage:`url(https://gmedia.playstation.com/is/image/SIEPDC/call-of-duty-modern-warfare-2-pack-01-ps4-ps5-en-26may22$en?$1200px$)`}}>
                <div className="img-container-overlay"></div>
              </div>
              <div className="description-container" >
                Call of Duty: Modern Warfare 2
              </div>
            </div>
            <div  className="card" >
              <div className="img-container" style={{backgroundImage:`url(https://gmedia.playstation.com/is/image/SIEPDC/call-of-duty-modern-warfare-2-pack-01-ps4-ps5-en-26may22$en?$1200px$)`}}>
                <div className="img-container-overlay"></div>
              </div>
              <div className="description-container" >
                Call of Duty: Modern Warfare 2
              </div>
            </div> */}
        </div>
        </div>
      </section>
    )
  }
  // return component()
  return query.isFetching
  ?<Spinner/>
  :component()
}

export default CustomCards