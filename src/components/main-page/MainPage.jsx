import React,{Suspense} from 'react'
import MainSlider from './main-slider/MainSlider';
import Spinner from '../../utilities/spinner/Spinner';
import CustomCards from './custom-cards/CustomCards';
import CompoundSlider from './compound-slider/CompoundSlider';
import CustomHero from './custom-hero/CustomHero';

const Home = () => {
  return (
    <Suspense fallback={<Spinner/>}>
        <CustomHero height="100vh" id={795632}/>
        <MainSlider/>
        <CustomHero animation="fade-right" height="70vh" id={326243}/> 
        <CustomCards  color="#000" background_color="transparent" title="Resident Evil series" query_key_series="resident-series" url="&page_size=5&search=resident-evil" series_url="&search=resident-evil"/>
        <CustomCards  color="#fff" background_color="#121314"  title="Gears of War series" query_key="gears-home" query_key_series="gears-series" url="&page_size=5&search=gears-of-war" series_url="&page_size=9&search=gears-of-war"/>
        <CustomHero animation="fade-right" height="100vh" id={799265}/>
        <CustomCards  color="#000" background_color="transparent" title="Shooters" query_key_series="shooters-home" url="&page_size=5&genres=2" series_url="&page_size=20&genres=2"/>
        <CustomCards  color="#fff" background_color="#151515" title="Fighting" query_key_series="fighting-home" url="&page_size=5&genres=6" series_url="&page_size=20&genres=6"/>  
        <CompoundSlider color="#fff" background_color="#151515" title="Slide" query_key_a="compound-slider-a" query_key_b="compound-slider-b" url_a="&page=2&page_size=12&genres=4" url_b="&page=6&page_size=12&genres=4"  />
    </Suspense>
  )
}

export default Home