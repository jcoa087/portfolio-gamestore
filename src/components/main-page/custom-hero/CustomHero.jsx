import React, { useEffect } from 'react'
import "./CustomHero.css"
import { useQuery } from 'react-query'
import Spinner from '../../../utilities/spinner/Spinner'    
import { fetchGame } from '../../../javascript/Apis'
import { useNavigate } from 'react-router-dom'
import AOS from "aos";
import "aos/dist/aos.css";
import Error from '../../../utilities/error/Error'
const CustomHero = (props) => {
    
    useEffect(() => {
        AOS.init({duration:1000,once:true});
        AOS.refresh();
      }, []);

    const navigate = useNavigate()
    const query = useQuery(["full-screen-game",props.id],()=>fetchGame(props.id))
    const set_game_data=()=>{
        navigate(`/game/${props.id}`)
    }
    if(query.isFetching)return <Spinner/>
    if(query.isError)return <Error/>

    return (
        <section id='custom-hero' style={{height:`${props.height}`}}>
            {/* <h1>{query.data.name}</h1> */}
            {props.animation?<h1 data-aos={props.animation}>{query.data.name}</h1>:<h1 >{query.data.name}</h1>}
            {props.animation?<button data-aos={props.animation}onClick={()=>set_game_data()}>View more</button>:<button onClick={()=>set_game_data()}>View more</button>}
            <img src={query.data.background_image} alt="" />
            {/* <img src="https://gmedia.playstation.com/is/image/SIEPDC/the-last-of-us-part-i-keyart-01-en-18may22?$1200px$" alt="" /> */}
        </section>
)
}

export default CustomHero