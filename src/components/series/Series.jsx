import React from 'react'
import "./Series.css"
import { useQuery } from "react-query";

import {useSeriesContext} from "../../context/Context"
import { useNavigate } from "react-router-dom";
import {fetchGeneric } from './../../javascript/Apis.js';
import Spinner from '../../utilities/spinner/Spinner'
const Series = () => {

  const[series,setSeries] = useSeriesContext()

  const navigate = useNavigate()
  console.log(series.url)

  const query = useQuery([series.query_key_series,series.url],()=>fetchGeneric(series.url))
  
  const set_game_data=(id)=>{
    navigate(`/game/${id}`)
  }
  const seriesComponent=()=>{
    return(
    <section className='series'>
      <h1>Resident Evil Series</h1>
      <div className="series-container">
      {
        query.data.results.map((game,idx)=>
        <div key={idx} className='game-card' style={{backgroundImage:`url(${game.background_image})`}} onClick={()=>set_game_data(game.id)} >
          <div className="game-title">{game.name}</div>
          <div className="game-overlay"></div>
        </div>
        )
      }
      </div>
    </section>
)
  }
  if(query.isFetching){return <Spinner/>}

  if(query.isError){return <p>Error fetching data</p>}
  return query.isFetching
  ?<Spinner/>
  :seriesComponent()
}

export default Series