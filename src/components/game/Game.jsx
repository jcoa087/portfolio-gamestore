import React, { useEffect } from 'react'
import "./Game.css"
import { useGalleryContext } from '../../context/Context'
import { useQuery } from 'react-query'
import Spinner from '../../utilities/spinner/Spinner'
import {fetchGame,fetchScreenshots,fetchTrailers} from "./../../javascript/Apis"
import Rating from '../../utilities/rating/Rating'
import ScreenShotSlider from './screenshot-slider/ScreenShotSlider'
import { useParams } from 'react-router-dom'
import Error from '../../utilities/error/Error'
const Game = () => {
  const params = useParams()
  const[gallery,setGallery] = useGalleryContext()
  const PC_PLATFORM=4
//Theres an error fetching data beacuse of the context, it uses the 0 on the url an it first tries to fetch that
  const query = useQuery({queryKey:["game",params.id],queryFn:()=>fetchGame(params.id)})
  const screenshots = useQuery({queryKey:["screenshots",params.id],queryFn:()=>fetchScreenshots(params.id)})
  let counter = 0
  if(query.isFetching || screenshots.isFetching){return <Spinner/>}

  if(query.isError || screenshots.isError){

    return <Error error_code={query.error.code} error_message={query.error.response.data.detail} error_detail={query.error.message} />}

  // const screenshots = [
  //   {img:"https://gmedia.playstation.com/is/image/SIEPDC/dead-space-launch-screenshot-03-en-18jan23?$1600px$"},
  //   {img:"https://gmedia.playstation.com/is/image/SIEPDC/dead-space-launch-screenshot-01-en-18jan23?$1600px$"},
  //   {img:"https://gmedia.playstation.com/is/image/SIEPDC/dead-space-screenshot-04-en-4oct22?$1600px$"},
  //   {img:"https://gmedia.playstation.com/is/image/SIEPDC/dead-space-launch-screenshot-02-en-18jan23?$1600px$"},
  //   {img:"https://gmedia.playstation.com/is/image/SIEPDC/dead-space-screeshot-01-en-09sep22?$1600px$"},
  //   {img:"https://gmedia.playstation.com/is/image/SIEPDC/dead-space-launch-screenshot-06-en-18jan23?$1600px$"}
  // ]
  const set_counter = (idx)=>{
    counter=idx

  }
  const loadedGameComponent = ()=>{
    return(
      <section id='game'>
        <div className="game-hero" style={{backgroundImage:`url(${query.data.background_image})`}} >
        {/* <div className="game-hero" style={{backgroundImage:`url(https://gmedia.playstation.com/is/image/SIEPDC/dead-space-pack-01-en-29sep22?$1200px$)`}} > */}
          <div className="game-overlay">
            <h2>{query.data.name}</h2>
            {/* <h2>Dead Space Remake</h2> */}
          </div>
        </div>
        <div className="game-info-container">
          <div className="game-info-content">
            <div className="top-left">
              <div className="game-description">
                <span>Game descripption</span>
                <br />
                <p>{
                  query.data.description_raw
                }</p>
                {/* <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium sunt eveniet necessitatibus porro temporibus delectus quidem animi doloribus, quae explicabo eligendi deleniti optio ut facere debitis tenetur quos, esse repellendus dolorum voluptatem dolores exercitationem velit placeat accusantium! Beatae ullam eveniet adipisci, amet necessitatibus, optio voluptatem expedita eos iusto, perferendis laboriosam.</p> */}
              </div> 
              <div className="additional-info">
                <div className="release">
                  <span>Release date</span>
                  <span>{query.data.released}</span>
                  {/* <span>2023-04-04</span> */}
                </div>
                <div className="publishers">
                  <span>Publishers</span>
                  <div className="publishers-content">
                    <span>Electronic Arts</span>
                  </div>
                </div>
                <div className="ratings">
                 
                  <span>Ratings</span>
                  <div className="ratings-content">
                    {/* <Rating background_color="#40AF3C" desc="Exceptional"/>
                    <Rating background_color="#315C82" desc="Recommended"/>
                    <Rating background_color="#DAAE31" desc="Meh"/>
                    <Rating background_color="#A42222" desc="Skip"/> */}
                      {
                        query.data.ratings.map((rating,idx)=>
                        <Rating key={idx}>{rating.title}<Rating width="100px" background_color="#44A142"/></Rating>
                        )
                      }  
                  </div>
                </div>
              </div>
            </div>
            <div className="top-right">

              <div className="tags">
                    <span>Tags</span>
                    <div className="tags-content">
                      {query.data.tags.map((tag,idx)=>
                      <span key={idx}>{tag.name}</span>
                      )}
                      {/* <span>Horror</span>
                      <span>Survival</span>
                      <span>Action</span>
                      <span>Third person shooter</span>
                      <span>Gore</span>
                      <span>Shooter</span> */}
                    </div>
              </div>

              <div className="platforms">
                <span>Platforms</span>
                <div className="platforms-content">
                {query.data.platforms.map((item,idx)=>
                <span key={idx}>{item.platform.name}</span>
                )}
                {/* <span>Xbox series X/S</span>
                <span>Playstation 5/4</span>
                <span>PC</span> */}
                </div>
              </div>
            
              <div className="genres">
                <span>Genres</span>
                <div className="genres-content">
                  {query.data.genres.map((genre,idx)=>
                  <span key={idx}>{genre.name}</span>
                  )}
                  {/* <span>Survival horro</span> */}
                </div>
              </div>
            </div>
            <div  className="middle">
              <span>PC Requirements</span>
              <div className="requirements-content">
              {query.data.platforms.map((requirement,idx)=>
              requirement.platform.id===PC_PLATFORM?<p className='min' key={idx}>{requirement.requirements?<li>{requirement.requirements.minimum}</li>:""}</p>:""
              )}
              {/* <p><span>Minimmum</span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum illum nisi quis deleniti provident optio natus neque ipsam, excepturi quam?</p>  */}
              {query.data.platforms.map((requirement,idx)=>
              requirement.platform.id===PC_PLATFORM?<p className='rec' key={idx}>{requirement.requirements?<li>{requirement.requirements.recommended}</li>:""}</p>:""
              )}
              {/* <p><span>Reccommended Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti quisquam voluptatibus ullam. Inventore, mollitia quisquam aperiam corrupti veritatis id vitae perspiciatis iste obcaecati consectetur. Expedita, ad nam? Possimus, necessitatibus debitis?</span></p> */}
              </div>
            </div>
            <div className="bottom-left">
              <div  className="screenshots" onClick={()=>setGallery(!gallery)}> 
                {
                  screenshots.data.results.map((item,idx)=>
                  <div className='screenshot' style={{backgroundImage:`url(${item.image})`}} alt="" key={idx} onClick={()=>set_counter(idx)}></div>
                  )
                }
{/*        
                {
                  screenshots.map((screenshot,idx)=>
                  <div key={idx} className="screenshot" style={{backgroundImage:`url(${screenshot.img})`}}></div>
                  )
                } */}
              </div>
            </div>
            <div className="bottom-right">
            </div>
          </div>
        </div>
        {gallery ? <ScreenShotSlider count={counter} screenshots={screenshots.data}/> : ""}
      </section>
    )
  }

  return loadedGameComponent()
  // return query.isFetching || screenshots.isFetching
  // ?<Spinner/>
  // :loadedGameComponent()
}

export default Game