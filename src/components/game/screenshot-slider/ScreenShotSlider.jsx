import React, { useState } from 'react'
import "./ScreenShotSlider.css"
import { useGalleryContext } from '../../../context/Context'
import { BsChevronRight,BsChevronLeft } from "react-icons/bs";
import { IoIosClose } from "react-icons/io";
const ScreenShotSlider = (props) => {
  const [gallery,setGallery]=useGalleryContext()
  const[cont,setCont] = useState(props.count)
  const movePrev=()=>{
    // let screenshots_slider=document.querySelector(".screenshots-slider")
    // screenshots_slider.style.transform="translateX(20%)"
    // setTimeout(() => {
    //   screenshots_slider.style.transform="translateX(0)"

    // }, 300);
    cont === 0?setCont(props.screenshots.results.length-1):setCont(cont=>cont-1)

  }

  const moveNext=()=>{
    // transform: translateX(100%);
    // let screenshots_slider=document.querySelector(".screenshots-slider")
    // screenshots_slider.style.transform="translateX(-20%)"
    // setTimeout(() => {
    //   screenshots_slider.style.transform="translateX(0)"

    // }, 300);
    cont === props.screenshots.results.length-1?setCont(0):setCont(cont=>cont+1)
  }
  return (
    <section className='screenshots-slider-overlay'>
      <button className='close' onClick={()=>setGallery(!gallery)}><IoIosClose/></button>
      <div className="controls">
        <div className="btn" onClick={()=>movePrev()}><BsChevronLeft className="icon"/></div>
        <div className="btn" onClick={()=>moveNext()}><BsChevronRight className="icon"/></div>
      </div>
      {/* <div className="screenshots-slider" style={{backgroundImage:`url(${props.screenshots[cont].background_image})`}}>
      </div> */}
      {
        props.screenshots.results?<div className="screenshots-slider" style={{backgroundImage:`url(${props.screenshots.results[cont].image})`}}>
        </div> :""
      }

    </section>
  )
}

export default ScreenShotSlider