import axios from "axios";
import {KEY} from "./Key"
import {BEARER} from "./igdb"
const BASE_URL="https://api.rawg.io/api/"
export const fetchGeneric = async (url="")=>{
  try{
    const res = await axios({
      url: `${BASE_URL}games?token&key=${KEY}${url}`,
      method: 'GET',
      // data: "fields name,background_image;"
    })
 
    return res.data
  }catch(error){
    console.error(error)
  }
}

export const fetchGame = async (id) => await axios
.get(`https://api.rawg.io/api/games/${id}?token&key=${KEY}`)
.then((res)=>{
  return res.data
})

export const fetchScreenshots = async(id)=> await axios
.get(`https://api.rawg.io/api/games/${id}/screenshots?token&key=${KEY}`)
.then((res)=>{
  return res.data
})

export const fetchTrailers = async(id)=> await axios
.get(`https://api.rawg.io/api/games/${id}/movies?token&key=${KEY}`)
.then((res)=>{
  return res.data
})

// export const fetchIGDB=async()=>{
//   const slug = "the-witcher-3-wild-hunt"
//   axios({
//     url: "https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/game_videos",
//     method: 'POST',
//     headers:{
//       "Client-ID":CLIENT_ID,
//       "Authorization":BEARER
//     },

//     data: "fields checksum,game,name,video_id;where video_id = 1690;"
//   })
//     .then(response => {
//         console.log(response.data);
//     })
//     .catch(err => {
//         console.error(err);
//     });
// }

// export const fetchScreenshots=async()=>{

//   axios({
//     url: "https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/screenshots",
//     method: 'POST',
//     headers:{
//       "Client-ID":CLIENT_ID,
//       "Authorization":BEARER
//     },

//     data: "fields alpha_channel,animated,checksum,game,height,image_id,url,width;where id = (9067, 9068, 9069, 9070, 9071);"
//   })
//     .then(response => {
//         console.log(response.data);
//     })
//     .catch(err => {
//         console.error(err);
//     });
// }

// export const fetchVideos=async()=>{

//   axios({
//     url: "https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/game_videos",
//     method: 'POST',
//     headers:{
//       "Client-ID":CLIENT_ID,
//       "Authorization":BEARER
//     },

//     data: "fields checksum,game,name,video_id;where video_id = 1690"
//   })
//     .then(response => {
//         console.log(response.data);
//     })
//     .catch(err => {
//         console.error(err);
//     });
// }

