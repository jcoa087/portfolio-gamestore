import React,{Suspense} from 'react'
import Hero from '../components/main-page/hero/Hero';
import MainSlider from '../components/main-slider/MainSlider';
import ConsoleCards from '../components/console-cards/ConsoleCards';
import NextGenConsoles from '../components/next-gen-consoles/NextGenConsoles';

import Spinner from '../utilities/spinner/Spinner';
const Home = () => {
  const SecondSlider = React.lazy(()=>import( "../components/second-slider/SecondSlider"))
  return (
    <Suspense fallback={<Spinner/>}>
        <Hero/>
        <MainSlider/>
        <ConsoleCards/>
        <NextGenConsoles/>

    </Suspense>
  )
}

export default Home