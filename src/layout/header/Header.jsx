import React from "react";
import "./Header.css"
import { ImSearch } from "react-icons/im";
import { useState } from 'react';
import { Link } from "react-router-dom";
const Header = ()=>{
    const [linkCont,setLinkCont] = useState(0)
    const [text,setText] = useState("")
    const links = [
    {text:"Home",id:1,path:"/"},

]
    return(
      <header>
        <div className="header-content">
          <div className="search-form">
            <ImSearch/>
            <div className="search-input-container">
              <input  type="text" placeholder="Search Games" onChange={e => setText(e.target.value)} />
            </div>
          </div>
          <div className="side-options">
            {
              links.map((link,idx)=>
                <Link to={link.path} key={idx} className={linkCont === link.id ? "game-link active" : "game-link"} onClick={()=>setLinkCont(link.id)} >{link.text}</Link>
              )
            }
          </div>
        </div>
      </header>
    )
}

export default Header