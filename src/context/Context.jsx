import React,{ createContext,useContext,useState,useEffect } from "react";


const gameContext = createContext()
const seriesContext = createContext()
const galleryContext = createContext()
const compoundSliderContext = createContext()



export const useSeriesContext = ()=>{
    return useContext(seriesContext)
}

export const useGalleryContext = ()=>{
    return useContext(galleryContext)
}

export const useCompoundSliderContext = ()=>{
    return useContext(compoundSliderContext)
}

export const Context =({children})=>{

    const [series,setSeries] = useState({url:"",query_key:""})
    const [gallery,setGallery] = useState(false)
    const [compoundSlider,setCompoundSlider] = useState({array_a:[],array_b:[]})
    
    return(

            <seriesContext.Provider value={[series,setSeries]}>
                <galleryContext.Provider value={[gallery,setGallery]}>
                    <compoundSliderContext.Provider value={[compoundSlider,setCompoundSlider]}>
                        {children}
                    </compoundSliderContext.Provider>
                </galleryContext.Provider>
            </seriesContext.Provider>

    )
}