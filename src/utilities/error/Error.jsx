import React, { useEffect, useState } from 'react'
import "./Error.css"
import { useNavigate } from 'react-router-dom'
import ErrorImg from "../../assets/error.jpg"
const Error = (props) => {
  const [cont,setCont]= useState(5)
  const navigate = useNavigate()
  const redirect = ()=>{
    setTimeout(()=>{
      navigate(`/`)
    },5000)
  }
  useEffect(()=>{
    setTimeout(() => {
      setCont(cont-1)
    }, 1000);
  },[cont])
  useEffect(()=>{
    redirect()
  },[])

  return (
    <div className='error' style={{backgroundImage:`url(${ErrorImg})`}}>
          <div className='error-content'>
            <div className='error-detail'>
              <h1>{props.error_code?props.error_code:"The page you were looking for does not exist"}</h1>
              <h2>{props.error_message?`${props.error_message} The information you were looking for is not available`:""}</h2>
              <h2>{props.error_detail?props.error_detail:""}</h2>
              <h3>You will be redirected in {cont}...</h3>
            </div>
          </div>
    </div>
  )
}

export default Error