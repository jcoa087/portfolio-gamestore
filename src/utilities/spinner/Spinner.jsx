import React from 'react'
import "./Spinner.css"
const Spinner = (props) => {
  return (
    <div className="spinner-background">
        <div className="spinner-overlay">
            <div className='spinner'></div>
            <span>Loading...</span>
        </div>
    </div>
  )
}

export default Spinner