import React from 'react'
import "./Rating.css"
const Rating = (props) => {

  return (
    <div className='rating' >
      <div className='color-rate' style={{backgroundColor:`${props.background_color}`}}></div>
      <span>{props.desc}</span>
    </div>
  )
}

export default Rating 