
import './App.css';
import Header from './layout/header/Header';
import Home from './components/main-page/MainPage';
import Game from './components/game/Game';
import Series from './components/series/Series';
import Screenshots from './components/screenshots/Screenshots';
import Error from './utilities/error/Error';
import { Route,Routes } from 'react-router-dom';
import { Context } from './context/Context';

function App() {
  
  return (
    <Context>
      <Header/>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/game/:id' element={<Game/>}/>
        <Route path='/series' element={<Series/>}/>
        <Route path='/screenshots' element={<Screenshots/>}/>
        <Route path="*" element={<Error />} />
      </Routes>
    </Context>
  );
}

export default App;
